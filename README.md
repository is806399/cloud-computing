# Cloud Computing

Module code: CSMCC16

Lecturer responsible: Atta Badii

CSMCC16: Cloud Computing 2020-2021

Coursework: Passenger Airline Flights

Student Number: 28806399

Date (when the work completed): 21/1/2021

This repository is created for Cloud Computing.

First, you need to install the packages using in project. Then, change the working dictionary of the project to make sure the file reader and writer able to process. 

You can use "python main.py" in your terminal to run the main functions like reading the input data and generating the number of passengers and so on.

"File.py" contains the function of reading the files, error checking, output writer.

"modify.py" contains the function of changing the time from unix time to HH:MM:SS.

"FileSplit.py" contains the funciton of spliting the files into apporiate size for reading.

"map_reduce.py" contains the function of map/reduce simulation.


The results are saved in "output" file, you can check the results after running.