from modify import modify_time
from FileSplit import FileSplit
from File import readPassengerData, readAirportData, writeFile
from map_reduce import FltNum, FltInfo, PassengerNumber, FltDists
# ------------------call function to achieve tasks-----------------
# 1) read the data from csv file
passenger_records = readPassengerData("./Data/AComp_Passenger_data.csv")
airport_records = readAirportData("./Data/Top30_airports_LatLong.csv")

# 2) calculate the number of flights from each airport
airport_flt_info = FltNum(passenger_records, airport_records)
airport_flt_info_path = "./output/airport_fltNum_infos.csv"

# 3) create the list of flights based on the flight id
flt_info = FltInfo(passenger_records)
flt_info_path = "./output/flt_infos.csv"

# 4) calculate the number of passengers on each flight
flt_passenger_info = PassengerNumber(passenger_records)
flt_passenger_info_path = "./output/flt_passengerNum_infos.csv"


# 5) calculate the nautical miles for each flight and the total miles for each passenger
flt_dists, passenger_dists = FltDists(passenger_records, airport_records)
flt_dists_path = "./output/flt_dists.csv"
passenger_dists_path = "./output/passenger_gain_dists.csv"
print("Passenger with ID_ '%s' has earned the highest air miles and traveled '%s' miles" % (passenger_dists[0][0], passenger_dists[0][1]))



#---------------------------output------------------------------
# get the output file  with defined function writeFile()
# 1) get the airport infomation "airport_flt_info"-------the number of flight in each airport
writeFile(airport_flt_info, airport_flt_info_path)

# 2) get the flight information "flt_info.csv" ----------flight list
writeFile(flt_info, flt_info_path)

# 3） get the passenger information "flt_passenger_info.csv" ---- number of passenger on each flight
writeFile(flt_passenger_info, flt_passenger_info_path)

# 4) get the distance of each flight
writeFile(flt_dists, flt_dists_path)

# 5)get the total miles for each passenger
writeFile(passenger_dists, passenger_dists_path)


# 6) change the unix time to gmt time
modify_time("./output/flt_infos.csv")
# spilt file to
# FileSplit("./Data/data_1.csv","mapfile")










