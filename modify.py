import pandas as pd


def modify_time(filename):
    ###insert header into csv and change the time
    csv = pd.read_csv(filename, header=None,
                      names=['Passenger_id', 'Flight_id', 'From', 'To', 'Departure_time', 'Arrival_time','flight_time'])
    csv['Departure_time'] = pd.to_datetime(csv['Departure_time'], unit='s', origin=pd.Timestamp('1970-1-1'))

    csv.to_csv(r'E:/Cloud Computing/output/flt_info_1.csv', index=False, header=False)

   # print(csv)

