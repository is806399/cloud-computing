import re

# import re to match the regular expression

# function readPassengerData to get the records from passsenger_data.csv
def readPassengerData(data_path):
    # data_path : ./Data/Acomp_Passenger_data.csv
    # to store the passenger records
    passenger_records = []
    records = set()
    error_records= set()
    print("reading the passenger records...")
    print("--------------------------------")
    with open(data_path, "r") as reader:
        line = reader.readline()
        line_num = 1

        while line:
            line = line.strip()
            line_num += 1
            # Unique checks
            if line in records:

                print("line %d is a duplicated data" % (line_num))

            # pattern matching checks
            else:
                cols = line.split(",")
                if not checkPassengerRecord(cols):
                    print("line %d is invalid data" % (line_num))
                else:
                    passenger_records.append(cols)
                    records.add(line)
            line = reader.readline()

    return passenger_records


def readAirportData(data_path):

    airport_records = []
    records = set() # to store the valid record line
    print("reading the airport records...")
    print("------------------------------")
    with open(data_path, "r") as reader:
        line = reader.readline()
        line_num = 0
        while line:
            line = line.strip()
            line_num += 1
            if line in records:
                print("line %d' is duplicated data" % (line_num))
            else:
                cols = line.split(",")
                if not checkFltRecord(cols):
                    print("line %d: is invalid data" % (line_num))
                else:
                    airport_records.append(cols)
            line = reader.readline()
    # return airport data, records format: [name, code, latitude, longitude]
    return airport_records

def checkPassengerRecord(record):


    # using regular expression to check the passenger record
    # regular expression parameters
    psg_id = "[A-Z]{3}\d{4}[A-Z]{2}\d"
    flt_id = "[A-Z]{3}\d{4}[A-Z]"
    code = "[A-Z]{3}"
    dpt_time = "\d{10}"
    flt_time = "\d{1,4}"
    # skip the missing values from the data
    if len(record) != 6:
        return False

    return checkValid(pattern=psg_id, s=record[0]) and checkValid(pattern=flt_id, s=record[1]) and checkValid(pattern=code, s=record[2]) and checkValid(pattern=code, s=record[3]) \
           and checkValid(pattern=dpt_time, s=record[4]) and checkValid(pattern=flt_time, s=record[5])



# for passenger information checking the records
def checkValid(pattern, s):

    pattern = "\A" + pattern + "\Z"
    return re.match(pattern, s)



# check the flight information is valid or not
def checkFltRecord(record):
    # skip the missing values from the data
    if len(record) != 4:
        return False

    name = "[A-Z]\s?{3,20}"
    code = "[A-Z]{3}"
    deg = "-?\d+\.\d+"

    # check the information fomat is correct or not #checkValid(pattern=name,s=record[0]) and
    return checkValid(code, s=record[1]) and checkValid(deg, s=record[2]) and checkValid(deg, s=record[3]) and len(record[2]) <= 13 and len(record[3]) <= 13




def writeFile(records, output_path):
   # write file into working dictionary

    with open(output_path, "w") as writer:
        for record in records:
            writer.write(",".join(record) + "\n")
