import os,os.path,time



def FileSplit(sourceFile, targetFolder):
    sFile = open(sourceFile, 'r')
    number = 100                    #
    dataLine = sFile.readline()
    tempData = []                           #
    fileNum = 1
    if not os.path.isdir(targetFolder):             #
        os.mkdir(targetFolder)
    while dataLine:                         #
        for row in range(number):
            tempData.append(dataLine)       #
            dataLine = sFile.readline()
            if not dataLine :                   #
                break
        tFilename = os.path.join(targetFolder,os.path.split(sourceFile)[1] + str(fileNum) + ".txt")
        tFile = open(tFilename, 'a+')           #
        tFile.writelines(tempData)              #
        tFile.close()
        tempData = []                       #
        print(tFilename + " 创建于: " + str(time.ctime()))
        fileNum += 1                        #
    sFile.close()

