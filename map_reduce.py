import time
import math


def FltInfo(passenger_records):
    # calculate flight information

    #------------------- map--------------------
    flt_info = dict()
    for record in passenger_records:
        passenger_id = record[0]
        flt_id = record[1]
        frm_code = record[2]
        dst_code = record[3]
        dpt_time = record[4]  # in Unix  1970-1-1
        flt_time = record[5]  # mins
        arv_time = ArvlTime(dpt_time, flt_time)
        output = [flt_id, passenger_id, frm_code, dst_code, dpt_time, arv_time, flt_time]
        if flt_id in flt_info:
            flt_info[flt_id].append(output)
        else:
            flt_info[flt_id] = [output]

    #------------------ reduce------------------

    flt_ids = flt_info.keys()
    flt_info_ = []
    for flt_id in flt_ids:
        records = flt_info[flt_id]
        records.sort()
        flt_info_.extend(records)

    return flt_info_

def FltNum(passenger_records, airport_records):
    # calculate the number of flights  from each airport

    # --------------------------map---------------------------
    airport_flt = dict()
    # passenger_records
    for record in passenger_records:
        flt_id = record[1]
        frm_code = record[2]
        if frm_code in airport_flt:
            airport_flt[frm_code].append(flt_id)
        else:
            airport_flt[frm_code] = [flt_id]



    # -------------------------reduce-------------------------

    airport_flt_info = dict()
    for record in airport_records:
        airport_code = record[1]
        airport_flt_info[airport_code] = record + ["0"]
    for airport_code in airport_flt:
        flt_num = len(airport_flt[airport_code])
        if airport_code in airport_flt_info:
            airport_flt_info[airport_code][-1] = str(flt_num)
        else:
            airport_flt_info[airport_code] = ["", airport_code, "", "", str(flt_num)]

    airport_flt_info_number = airport_flt_info.values()

    return airport_flt_info_number


def ArvlTime(dpt_time, flt_time):
    # get the arrival time
    time1 = time.gmtime(int(dpt_time) + int(flt_time))
    time1 = time.strftime("%H:%M:%S", time1)
    return time1




def PassengerNumber(passenger_records):

    # calculate the number of passengers on each flight

    # -------------------------------map-----------------------
    flt_info = dict()
    for record in passenger_records:
        passenger_id = record[0]
        flt_id = record[1]
        if flt_id in flt_info:
            flt_info[flt_id].append(passenger_id)
        else:
            flt_info[flt_id] = [passenger_id]

    # ------------------------------reduce----------------------

    flt_ids = flt_info.keys()
    flt_info_ = []
    for flt_id in flt_ids:
        passenger_num = len(flt_info[flt_id])
        flt_info_.append([flt_id, str(passenger_num)])

    return flt_info_


def distance(lat1, lon1, lat2, lon2):

    # d(x1,y1,x2,y2)=r*arccos(sin(x1)*sin(x2)+cos(x1)*cos(x2)*cos(y1-y2))x1

    t = abs(lon1 - lon2)  # angle between lon1 and lon2
    dist = math.sin(deg2rad(lat1)) * math.sin(deg2rad(lat2)) + math.cos(deg2rad(lat1)) * math.cos(deg2rad(lat2)) * math.cos(deg2rad(t))
    dist = math.acos(dist)
    dist = rad2deg(dist) # get the degrees between two airports
    miles = dist * 11132/1609 # miles to degree *11132/1609
    return miles


def deg2rad(degree):

    return degree / 180 * math.pi


def rad2deg(radian):

    return radian * 180 / math.pi


def FltDists(passenger_records, airport_records):

    airport_info = dict()
    for airport_record in airport_records:
        airport_code = airport_record[1]
        lat = float(airport_record[2])
        lon = float(airport_record[3])
        airport_info[airport_code] = [lat, lon]

    # ------------------------map------------------------------
    flt_dists = dict()
    passenger_dists = dict()
    for record in passenger_records:
        flt_id = record[1]
        frm_code = record[2]
        dst_code = record[3]
        if flt_id not in flt_dists:
            frm_lat, frm_lon = airport_info[frm_code]
            dst_lat, dst_lon = airport_info[dst_code]
            dist = distance(frm_lat, frm_lon, dst_lat, dst_lon)
            flt_dists[flt_id] = dist

        passenger_id = record[0]
        if passenger_id not in passenger_dists:
            passenger_dists[passenger_id] = [dist]
        else:
            passenger_dists[passenger_id].append(dist)

    flt_dists_ = []
    flt_ids = flt_dists.keys()
    for flt_id in flt_ids:
        flt_dists_.append([flt_id, "%.f" % flt_dists[flt_id]])

    # ---------------------------------reduce---------------------------

    # calculate the total miles for each passenger
    passenger_dist_ = []
    for passenger_id in passenger_dists:
        total_dist = sum(passenger_dists[passenger_id])
        passenger_dist_.append([passenger_id, "%.f" % total_dist])

    passenger_dist_.sort(key=lambda x: float(x[1]), reverse=True)

    return flt_dists_, passenger_dist_
